<!doctype html>
<html>
<head>
	<title>Update d'un mot</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

</head>
<body>
	<nav>
		<ul class="nav nav-tabs">
<li role="presentation" class="active"><a href="index.php">Home</a></li>
<li role="presentation"><a href="nom.php">Joueurs</a></li>
<li role="presentation"><a href="mot.php">Mots</a></li>
	 </ul>

	</nav>

      <form action="update-mot.php" method="GET">
         <label for="mot">Modifier mot :</label>
				 <div class="form-group">
	     <input type="text" class="form-control" placeholder="nouveau mot">
         <input type="hidden" name="Id" value="<?php echo $_GET["Id"]; ?>" >
			 </div>
			 <button type="submit" class="btn btn-success">Valider</button>
		 </form>

   <br>
<a class="btn btn-primary" href="mot.php" role="button">Retour</a>
</body>
</html>
